<?php get_header(); ?>
<div class="uk-container uk-container-center archive">
	<div class="uk-grid">
		<div class="uk-width-medium-7-10">
		<h1>Blog</h1>
		<?php while(have_posts()) : the_post(); ?>
			<?php get_template_part("loop", "archive"); ?>
		<?php endwhile; ?>
		<?php get_template_part("pagination"); ?>
		</div>
		<div class="uk-width-medium-3-10 uk-hidden-small">
			<?php get_sidebar("primary"); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>