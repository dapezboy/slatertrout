<article class="uk-article">
	<div class="uk-panel-box">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail("thumbnail", array("class" => "uk-thumbnail uk-align-left"));  ?>
			<h2 class="uk-article-title uk-margin-remove"><?php the_title(); ?></h2>
		</a>
		<div class="uk-article-meta uk-margin-small-bottom">Published <?php the_time(get_option("date_format")); ?> at <?php the_time(); ?></div>
		<?php the_excerpt(); ?>
		<div class="uk-text-right"><a href="<?php the_permalink(); ?>" class="readmore">Read More <i class="uk-icon-arrow-right"></i></a></div>
	</div>
</article>