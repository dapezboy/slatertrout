<?php get_header(); ?>
<div class="uk-container uk-container-center single">
	<div class="uk-grid">
		<div class="uk-width-medium-7-10">
			<?php while(have_posts()) : the_post(); ?>
				<?php get_template_part("loop", "single"); ?>
			<?php endwhile; ?>
		</div>
		<div class="uk-width-medium-3-10">
			<?php get_sidebar("primary"); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>