<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title><?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo( "name", "raw"); ?></title>

		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

        <link href="<?php echo get_theme_part("css", "base.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_theme_part("css", "addons.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_theme_part("css", "bxslider.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_theme_part("css", "fancybox.css"); ?>" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo get_theme_part(null, "style.css"); ?>" rel="stylesheet" type="text/css" />
        <link href='//fonts.googleapis.com/css?family=Merriweather:400,700|Raleway:400,700|Arvo:700' rel='stylesheet' type='text/css'>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo get_theme_part("js", "base.js"); ?>"></script>
        <script src="<?php echo get_theme_part("js/addons", "sticky.min.js"); ?>"></script>
        <script src="<?php echo get_theme_part("js/addons", "jquery.bxslider.js"); ?>"></script>
        <script src="<?php echo get_theme_part("js/addons", "jquery.fitvids.js"); ?>"></script>
        <script src="<?php echo get_theme_part("js/addons", "jquery.fancybox.js"); ?>"></script>
        <script src="<?php echo get_theme_part("js/addons", "jquery.cookie.js"); ?>"></script>

        <link rel="shortcut icon" href="/favicon.ico">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php 
            wp_head();
            if(isset($_COOKIE['background'])) {
                $bgs = array(
                    "post_type" => "bg_image",
                    "p" => $_COOKIE['background']
                );
            } else {
                $bgs = array(
                    "post_type" => "bg_image",
                    "orderby" => "rand",
                    "posts_per_page" => 1
                );
            }
            $the_query = new WP_Query($bgs); 
            while($the_query->have_posts()) : $the_query->the_post();
                $cimg = get_post_meta($post->ID, "color_photo", true); $cimg = $cimg['guid'];
                $bwimg = get_post_meta($post->ID, "bw_photo", true); $bwimg = $bwimg['guid'];
                $idimg = $post->ID;
            endwhile;
        ?>
    </head>
    <body id="top" <?php body_class(); ?> style="<?php if(!isset($_COOKIE['background'])) : ?>overflow: hidden; <?php endif; ?> background-image: url('<?php echo $bwimg; ?>')">
    <?php if(!isset($_COOKIE['background'])) : ?>
        <div class="loading-image" style="background-image: url('<?php echo $cimg; ?>');">
            <div class="logoa">
                <i class="uk-icon-spinner uk-icon-spin uk-icon-large uk-text-center"></i>
            </div>
        </div>
        <div class="loading-image2" style="background-image: url('<?php echo $bwimg; ?>');"></div>
        <script>
            $(window).load(function() {
                window.setTimeout(overlayFadeOut, 1000);
            });
            function overlayFadeOut() {
                $('.uk-container.home').css('margin-top', '50vh');
                $('.loading-image').fadeOut('800', function() {
                    $(this).remove();
                    $('.loading-image2').fadeOut('800', function() {
                        $(this).remove();
                        $('body').css('overflow', 'auto');
                        $.cookie('background', '<?php echo $idimg; ?>', { path: '/' });
                    });
                });
            }
            $(window).scroll(function() {
                if($(window).scrollTop() > 130) {
                    $('.uk-container.home').animate({'margin-top': '0'});
                }
            });
        </script>
    <?php endif; ?>
    <?php
        $menuargs = array(
            "container"         => "",
            "theme_location"    => "primary_nav",
            "menu_class"        => "uk-navbar-nav uk-float-right uk-visible-large",
            "menu_id"           => "",
            "depth"             => 2,
            "walker"            => new Walker_UIKIT
        );
        $offcanvnav = array(
            "container"         => "",
            "theme_location"    => "primary_nav",
            "menu_class"        => "uk-nav uk-nav-offcanvas",
            "menu_id"           => ""
        );
    ?>
    <header class="uk-margin-bottom">
        <div class="header uk-visible-large" data-uk-sticky="{top:-120, animation: 'uk-animation-slide-top'}">
            <div class="uk-container uk-container-center">
                <nav class="uk-navbar">
                    <div class="uk-navbar-content"><a class="no-fade" href="<?php echo home_url(); ?>">Slater Trout</a></div>
                    <?php wp_nav_menu($menuargs); ?>
                </nav>
            </div>
        </div>
        <div id="off-canv" class="uk-offcanvas">
            <div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
                <?php wp_nav_menu($offcanvnav); ?>
            </div>
        </div>
        <div class="uk-container uk-container-center logo-cont">
            <div class="logo">
                <a class="no-fade" href="<?php echo home_url(); ?>"><img src="<?php echo get_theme_part("images", "slatertrout-logo.png"); ?>" alt="SlaterTrout" /></a>
            </div>
            <a href="#off-canv" class="uk-float-right uk-hidden-large uk-navbar-toggle" data-uk-offcanvas></a>
        </div>
    </header>