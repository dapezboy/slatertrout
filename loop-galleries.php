<div class="uk-width-small-1-2">
	<article class="uk-article">
		<div class="uk-panel-box">
			<a href="<?php the_permalink(); ?>">
			<?php 
				if(has_post_thumbnail()) {
					the_post_thumbnail("gallery-banner", array("class" => "uk-thumbnail uk-thumbnail-expand")); 
				} else {
					$images = get_post_meta($post->ID, "images");
					echo wp_get_attachment_image($images[0]["ID"], "gallery-banner", false, array("class" => "uk-thumbnail uk-thumbnail-expand"));
				}
			?>
				<h3 class="uk-margin-top-remove"><?php the_title(); ?></h3>
			</a>
		</div>
	</article>
</div>