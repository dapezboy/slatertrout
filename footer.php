	<div class="sponsors">
		<div class="uk-container uk-container-center">
			<ul class="bxslider">
				<?php
					$slider = array(
						"post_type"			=> "sponsor",
						"posts_per_page"	=> -1,
					);
					$the_query = new WP_Query($slider);
					while ($the_query->have_posts()) : $the_query->the_post();
					$image = get_post_meta(get_the_ID(), "slide_image", TRUE);
				?>
					<li><a href="<?php echo get_post_meta(get_the_ID(), "_sponsor", true); ?>" target="_blank"><?php echo wp_get_attachment_image($image["ID"],"full", array("class"=>"uk-responsive-height")); ?></a></li>
				<?php endwhile; ?>
			</ul>
		</div>
	</div>
	<footer class="uk-text-center">
		<div class="uk-container uk-container-center copy">&copy; 2014 <?php echo get_bloginfo( "name", "raw"); ?>. All Rights Reserved. &bull; Designed by <a href="http://goodjustworks.org/" target="_blank">Just Works</a><!-- Developed by <a href="http://ankemp.com">Andrew Kemp</a> --></div>
	</footer>
	<?php wp_footer(); ?>
	<script>
		$(".video-thumb").click(function(event) {
			event.preventDefault();
			var href = $(this).attr("href");
			var height = $(this).find("img").height();
			var videoTag = '<iframe class="uk-width-1-1" height="'+height+'px" src="'+href+'&autoplay=1" frameborder="0" allowfullscreen></iframe>';
			$(this).replaceWith(videoTag);
		});
		$('.sponsors .bxslider').bxSlider({
			minSlides: 4,
			maxSlides: 6,
			moveSlides: 1,
			slideWidth: 192,
			slideMargin: 0,
			pause: 5000,
			mode: 'horizontal',
			auto: true,
			autoHover: true,
			pager: false,
			controls: false,
			preloadImages: 'all'
		});
	</script>
	</body>
</html>