<?php get_header(); ?>
	<div class="uk-container uk-container-center page">
		<?php while(have_posts()) : the_post(); ?>
			<article class="uk-article">
				<h1 class="uk-article-title"><?php the_title(); ?></h1>
				<div class="uk-panel-box"><?php the_content(); ?></div>
			</article>
		<?php endwhile; ?>
	</div>
<?php get_footer(); ?>