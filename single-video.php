<?php get_header(); ?>
<div class="uk-container uk-container-center single">
	<?php while(have_posts()) : the_post(); ?>
		<?php get_template_part("loop", "video"); ?>
	<?php endwhile; ?>
</div>
<?php get_footer(); ?>