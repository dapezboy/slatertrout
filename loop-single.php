<article class="uk-article">
	<h1 class="uk-article-title"><?php the_title(); ?></h1>
	<div class="uk-panel-box">
		<?php the_post_thumbnail("post-banner", array("class" => "uk-thumbnail-expand uk-margin-bottom"));  ?>
		<div class="uk-article-meta uk-margin-small-bottom">
			Published <?php the_time(get_option("date_format")); ?> at <?php the_time(); ?> in <?php the_category(", ", "single"); ?>
		</div>
		<?php the_content(); ?>
		</div>
</article>