<?php get_header(); ?>
<div class="uk-container uk-container-center archive videos">
	<h1>Videos</h1>
	<div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{row: true, target: '.uk-panel-box'}">
		<?php while(have_posts()) : the_post(); ?>
			<?php get_template_part("loop", "videos"); ?>
		<?php endwhile; ?>
		<?php get_template_part("pagination"); ?>
	</div>
</div>
<?php get_footer(); ?>