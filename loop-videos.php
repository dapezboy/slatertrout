<div class="uk-width-medium-1-2">
	<article class="uk-article">
		<div class="uk-panel-box">
			<?php $video = get_video($post->ID); ?>
			<a class="video-thumb" href="<?php echo $video['href']; ?>">
				<img class="uk-thumbnail uk-thumbnail-expand" src="<?php echo $video['img']; ?>" />
			</a>
			<a href="<?php the_permalink(); ?>"><h3 class="uk-margin-top-remove"><?php the_title(); ?></h3></a>
		</div>
	</article>
</div>