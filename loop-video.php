<article class="uk-article">
	<div class="uk-panel-box">
		<?php $video = get_video($post->ID); ?>
		<iframe class="uk-height-viewport uk-width-1-1" src="<?php echo $video['href']; ?>" frameborder="0" allowfullscreen></iframe>
		<h3 class="uk-article-title uk-margin-top-remove"><?php the_title(); ?></h3>
		<div class="uk-text-right uk-text-small"><a href="<?php echo $video['link']; ?>" target="_blank"><i class="uk-icon-external-link"> Watch on <?php echo $video['host']; ?></i></a></div>
		<?php the_content(); ?>
	</div>
</article>
