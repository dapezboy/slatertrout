<?php get_header(); ?>
	<div class="uk-container uk-container-center home">
		<div class="uk-grid">
			<div class="uk-width-medium-7-10">
				<article class="uk-article">
					<a href="./videos"><h2 class="uk-article-title">Videos</h2></a>
					<div class="uk-panel-box">
						<?php
							$videos = array(
								"post_type" => "video",
								"posts_per_page" => 1
							);
							$the_query = new WP_Query($videos); 
							while($the_query->have_posts()) : $the_query->the_post(); $video = get_video($post->ID); ?>
							<a class="video-thumb " href="<?php echo $video['href']; ?>">
								<img class="uk-thumbnail uk-thumbnail-expand" src="<?php echo $video['img']; ?>" />
							</a>
							<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
						<?php endwhile; ?>
					</div>
					<div class="uk-text-right uk-margin-top">
						<a href="./videos" class="readmore">View all Videos <i class="uk-icon-arrow-right"></i></a>
					</div>
				</article>
				<article class="uk-article">
					<a href="./blog"><h2 class="uk-article-title">Happenings</h2></a>
					<div class="uk-grid" data-uk-grid-margin data-uk-grid-match>
						<?php
							$posts = array(
								"post_type" => "post",
								"cat" => 13,
								"posts_per_page" => 6
							);
							$the_query = new WP_Query($posts); 
							while($the_query->have_posts()) : $the_query->the_post(); ?>
							<div class="uk-width-small-1-2">
								<div class="uk-panel-box">
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail("post-homethumb", array("class" => "uk-thumbnail uk-thumbnail-expand"));  ?>
										<h3><?php the_title(); ?></h3>
									</a>
									<?php the_excerpt(); ?>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
					<div class="uk-text-right uk-margin-top">
						<a href="./blog" class="readmore">View the Blog <i class="uk-icon-arrow-right"></i></a>
					</div>
				</article>
				<article class="uk-article">
					<a href="./galleries"><h2 class="uk-article-title">Photo Galleries</h2></a>
						<div class="uk-grid" data-uk-grid-margin data-uk-grid-match>
							<?php
								$gallery = array(
									"post_type" => "gallery",
									"posts_per_page" => 4
								);
								$the_query = new WP_Query($gallery); 
								while($the_query->have_posts()) : $the_query->the_post(); ?>
								<div class="uk-width-small-1-2">
									<div class="uk-panel-box">
										<a href="<?php the_permalink(); ?>">
										<?php 
											if(has_post_thumbnail()) {
												the_post_thumbnail("post-homethumb", array("class" => "uk-thumbnail uk-thumbnail-expand")); 
											} else {
												$images = get_post_meta($post->ID, "images");
												echo wp_get_attachment_image($images[0]["ID"], "post-homethumb", false, array("class" => "uk-thumbnail uk-thumbnail-expand"));
											}
										?>
											<h3><?php the_title(); ?></h3>
										</a>
										<?php the_excerpt(); ?>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
						<div class="uk-text-right uk-margin-top">
							<a href="./galleries" class="readmore">View all Galleries <i class="uk-icon-arrow-right"></i></a>
						</div>
					
				</article>
			</div>
			<div class="uk-width-medium-3-10 uk-hidden-small">
				<?php get_sidebar("primary"); ?>
			</div>
		</div>
	</div>
	</div>

<?php get_footer(); ?>