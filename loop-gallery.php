<article class="uk-article">
	<h1 class="uk-article-title"><?php the_title(); ?></h1>
	<div class="uk-panel-box">
		<div class="uk-grid" data-uk-grid-margin>
		<?php foreach(get_post_meta(get_the_ID(), "images") as $image) : ?>
			<div class="uk-width-medium-1-5 uk-width-small-1-3 uk-width-1-2">
				<a class="fancybox" rel="<?php the_title(); ?>-gallery" href="<?php echo $image["guid"]; ?>">
					<?php echo wp_get_attachment_image($image["ID"], "thumbnail", false, array("class" => "uk-thumbnail uk-thumbnail-expand")); ?>
				</a>
			</div>
		<?php endforeach; ?>
		<script>
		$(document).ready(function() {
			$(".fancybox").fancybox({
				openEffect	: 'fade',
				closeEffect	: 'fade'
			});
		});
		</script>
		</div>
	</div>
</article>