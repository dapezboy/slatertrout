<ul class="uk-pagination uk-margin-top">
	<li class="uk-pagination-next"><?php next_posts_link("Previous <i class='uk-icon-angle-double-right'></i>"); ?></li>
	<li class="uk-pagination-previous"><?php previous_posts_link("<i class='uk-icon-angle-double-left'></i> Next"); ?></li>
</ul>